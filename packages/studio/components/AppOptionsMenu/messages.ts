import { defineMessages } from 'react-intl';

export const messages = defineMessages({
  exportText: {
    id: 'studio.SVwJTM',
    defaultMessage: 'Export',
  },
  exportWithResources: {
    id: 'studio.ny2yoG',
    defaultMessage: 'Include resources in the export file.',
  },
});
