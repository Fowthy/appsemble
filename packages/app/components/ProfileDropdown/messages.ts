import { defineMessages } from 'react-intl';

export const messages = defineMessages({
  logoutButton: {
    id: 'app.C81/uG',
    defaultMessage: 'Logout',
  },
  demoLogin: {
    id: 'app.hMd0Ls',
    defaultMessage: 'Demo Login: Change Role',
  },
  feedback: {
    id: 'app.Ejhdi4',
    defaultMessage: 'Feedback',
  },
  settings: {
    id: 'app.D3idYv',
    defaultMessage: 'Settings',
  },
  pfp: {
    id: 'app.IKMcY6',
    defaultMessage: 'Profile Picture',
  },
  login: {
    id: 'app.AyGauy',
    defaultMessage: 'Login',
  },
});
